FROM php:7.4-apache
RUN docker-php-ext-install mysqli
COPY . /var/www/html/
ENV DB_HOST=mysql-service
ENV DB_USER=ecomuser
ENV DB_PASSWORD=ecompassword
ENV DB_NAME=ecomdb
EXPOSE 80
